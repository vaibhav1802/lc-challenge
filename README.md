##LC CHALLENGE
This project was created using React's create-app utility.

Below are the dependencies this project is using:

1) React Router: For routing

2) Jest and Enzyme: For unit test

3) Google Maps React Lib:
https://www.npmjs.com/package/google-maps-react

4) FB Emitter - Event
This event base channel has been used to connect to different components and passing
the data from one to another. My preference would have been Redux and connect the propertDetail component with redux store, but that would have been a lot of work for a sample code.

5) Polyfill native fetch using whatwg-fetch 
In the example, we have mock data, so the whole implementation couldn't be done.

6) Proptypes for typechecking

7) Node sass for sass style compiler


## THINGS THAT I COULD HAVE DONE BETTER 
Below are the areas which I would have done better with more time:

1) Styling : I would have used some CSS transitions and filter toggling to make sure
the application looks better on Mobile

2) More Unit Tests: I wrote few sample unit tests which includes component and javascript logic test cases. I could have covered more in components.

3) Redux implementation: I am using Event emmitter to pass the data between components. In some 
cases, like active filter applied, redux would have been a good tool to interact between components via store.

## REQUIREMENTS
Landchecker (LC) FrontEnd Coding Challenge
This code test is your opportunity to demonstrate to us how you think in a relaxed and familiar environment. The test is derived from a real world scenario that we have in LC.
We will be looking to understand how you develop, architect or structure your code? format code? Approach to testing? how you use Git? CSS? how to make use of your limited time? etc.
Please ensure to commit the repo in github or bitbucket so we can see the commit history.
Time Limit
We expect you to spend between 2-6 hours to complete this challenge.
Delivery
Email us a link to the repo.
Challenge
Display properties as markers on a map using reactjs.
1. Feel free to mock the JSON payload. See samples/properties.json
     {
       "id": 1525674,
       "lga_code": 311,
       "council_property_number": 2188100300,
       "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
       "council": "CARDINIA",
       "postcode": 3782,
       "latitude": 145.449895817713,
       "longitude": -37.9373447811622
}
2. Create a front-end application in reactjs which consumes the endpoint and display the markers on the map. You can either use mapbox or google maps for mapping. It would be good to see some tests as well.
3. When I click on a property marker, display the property details on the right hand side of the page similar to the LC website. See http://landchecker.com.au/map/property/50071791
We don’t expect to see all the property details section based on the example link. Showing the full_address, council, postcode based on the sample data will suffice for this test. If you want to mock other parts of the property details section, feel free to do so.
4. Filters. Add the ability to filter by council on the left side of the page. If you want to add more to the challenge, feel free to do so. Good luck.


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.