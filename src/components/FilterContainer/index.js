import React, {Component} from 'react';
import {filterUniqueData, filterOutData} from '../../utilities/filter';
import { default as eventHub }  from '../../eventHub';
import mockData from '../../mock-data/properties.json';
import PropTypes from 'prop-types';

class FilterContainer extends Component {
  constructor(props) {
    super(props);
    this.appliedFilterItems = [];
    this.emitter = eventHub.getInstance();
  }

  selectItem(event) {
    if (event.currentTarget.className === 'selector non-active') {
      this.appliedFilterItems.push(event.currentTarget.id);
      return event.currentTarget.className = 'selector active';
    } 
    
    this.appliedFilterItems.pop(event.currentTarget.id);
    return event.currentTarget.className = 'selector non-active';
  }

  onFilterApply() {
    let filteredResult = filterOutData(mockData, this.appliedFilterItems);
    /*Emmit the filtered data to map component
    Ideally this should happen through redux store by dispatching an action with payload of
    filtered data*/
    this.emitter.emit('FILTER_APPLIED', filteredResult);
  }

  render() {
    const {title, filterField} = this.props;
    const uniqueFilterItems = filterUniqueData(mockData, filterField);
   
    return (
      <div className="filter-container">
        <h4 className="title">{title}</h4>
        <ul>
        {
          uniqueFilterItems.map((item, index) => 
            <li className="filter-list" key={index}>
              <div className="filter-item">
                <span id={item} className="selector non-active" onClick={(e) => this.selectItem(e)}></span>
                <span className="item-name">{item}</span>
              </div>
            </li>
          )
        }
        </ul>
       <button className="btn filter-apply" onClick={() => this.onFilterApply()}>APPLY</button>
     </div>
    )
  }
};

FilterContainer.propTypes = {
  title: PropTypes.string,
  filterField: PropTypes.string
};

export default FilterContainer;