import React from 'react';
import PropTypes from 'prop-types';
import NavBar from '../NavBar';
import HeaderLinkConfig from '../../config/config.json';

// Instead of hard coding the a tag links, lets pass that through config file for flexibility
const Header = props => {
  return (
    <header className="header-wrapper">
      <a className="logo-wrapper" href="#map">
        <img className="img-logo" src={props.logo} alt="logo-landcheker"></img>
      </a>
      <NavBar links={HeaderLinkConfig.headerComponentLinks}/>
    </header>
  )
};

Header.propTypes = {
  logo: PropTypes.string,
  links: PropTypes.array
};

export default Header;