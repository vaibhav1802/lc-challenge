import React from 'react';
import NavBar from './index.js';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

let saved = {};

describe('<NavBar />', () => {
  describe('Check navbar exist with no data', () => {
    let testData = [];
    beforeEach(() => {
      saved = {
        wrapper: shallow(<NavBar links={testData}/>)
      }
    });
    afterEach(() => {
      saved.wrapper.unmount();
      saved = {};
    });
    test('Navbar length with no data', () => {
      expect(saved.wrapper.find('.nav-link').length).toBe(0);
    });
  });
  describe('Check navbar exist with data', () => {
    let testData = [
      {
        "label": "Map",
        "href": "#map",
        "classTag": "link-map"
      },
      {
        "label": "Account",
        "href": "#account",
        "classTag": "link-account"
      },
      {
        "label": "Site Finder",
        "href": "#site",
        "classTag": "link-site-finder"
      }
    ];
    beforeEach(() => {
      saved = {
        wrapper: shallow(<NavBar links={testData}/>)
      }
    });
    afterEach(() => {
      saved.wrapper.unmount();
      saved = {};
    });
    test('Navbar length with data', () => {
      expect(saved.wrapper.find('.nav-link').length).toBe(3);
    });
  });
});
