import React from 'react';
import PropTypes from 'prop-types';

const NavBar = props => {
  return (
    <nav className="nav-options">
      { props.links.length && 
        props.links.map((link, index) => 
          <a key={index} href={link.href} className={`nav-link ${link.classTag}`}>{link.label}</a>
        )
      }
    </nav>
  )
};

NavBar.propTypes = {
  links: PropTypes.array
};

export default NavBar;