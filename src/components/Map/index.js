import React, { Component } from 'react';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import { default as eventHub }  from '../../eventHub';
import mockData from '../../mock-data/properties.json';

const MAP_API_KEY = "AIzaSyBigVOQjIQeEzkKlJTMpmciq_5E8x7Aobw";

class MapComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {updateFilteredData: false};
    this.properties = mockData;
    this.onMarkerClick = this.onMarkerClick.bind(this);
  }

  componentDidMount() { 
    // Add the listener when the filter is applied in filter component
    this.emitter = eventHub.getInstance();
    this.notificationListener = this.emitter.addListener('FILTER_APPLIED', (options) => this.filterProperty(options));
  }

  filterProperty(options) {
    this.properties = options;
    this.setState({
      updateFilteredData: true
    });
  }

  onMarkerClick(event) {
    // Pass the property info that was clicked
    // This can be handled through Redux as well, instead of event emitter
    this.emitter.emit('PROPERTY_CLICKED', event);
  }

  render() {
    return(
      <div className='map-wrapper'>
        <Map google={this.props.google} 
          style={{width: '100%', height: '100%', position: 'relative'}}
          initialCenter={{
            lat: -40.854885,
            lng: 144.96332
          }}
          zoom={5}> 
          {
            this.properties.length &&
            this.properties.map((property, index) => 
            <Marker onClick={(e) => this.onMarkerClick(e)}
              position={{lat: property.latitude, lng: property.longitude}}
              id={property.property_id}
              key={index}
              address={property.full_address}
              council={property.council}
              postcode={property.postcode}
            /> 
            )
          }
        </Map>
      </div>
    );
  }
};

export default GoogleApiWrapper({
  apiKey: (MAP_API_KEY)
})(MapComponent)