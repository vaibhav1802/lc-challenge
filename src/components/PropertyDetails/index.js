import React, {Component} from 'react';
import { default as eventHub }  from '../../eventHub';

export default class PropertyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDetailsOpen: false
    };
  }
  componentDidMount() {
    // Add the listener for property clicked event
    this.emitter = eventHub.getInstance();
    this.notificationListener = this.emitter.addListener('PROPERTY_CLICKED', (options) => this.showDetails(options));
  }

  showDetails(data) {
    this.options = data;
    this.setState({
      isDetailsOpen: true
    });
  }

  closeDetails() {
    this.setState({
      isDetailsOpen: false
    });
  }

  render() {
    return(
      this.state.isDetailsOpen ?
      <div className="property-details-wrapper">
        <h4 className="title">Property Details <span className="close-icon" onClick={()=> this.closeDetails()}>&times;</span></h4>
        <div className="property-info">
          <div className="address">
            <span className="label">Address:</span>
            <span>{this.options.address}</span>
          </div>
          <div className="council">
            <span className="label">Council:</span>
            <span>{this.options.council}</span>
          </div>
          <div className="post">
            <span className="label">Post Code:</span>
            <span>{this.options.postcode}</span>
          </div>
        </div>
      </div>
      : null
    );
  }
}