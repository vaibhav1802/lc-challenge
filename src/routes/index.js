import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import LandingPage from '../pages/Landing';
import AccountPage from '../pages/Account';

const applicationRoutes = (
  <Switch>
    <Route name="map" path="/map" component={LandingPage} />
    <Route name="account" path="/account" component={AccountPage} />
    <Redirect exact from="/" to="map" />
  </Switch>
);

export default applicationRoutes;
