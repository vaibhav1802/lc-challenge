import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.scss';
import App from './app';

ReactDOM.render(<App />, document.getElementById('root'));
