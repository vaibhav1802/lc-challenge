/* Since I am using native fetch, I would need some polyfill for older browsers. whatwg-fetch is a 
good choice*/
import 'whatwg-fetch';

// This is the service to fetch the properties data from the back end and return the promise object
export function fetchProperties(path) {
  let req = new Request(path, {
    credentials: 'include'
  });
  return fetch(req)
  .then((response) => {
    return response.json();
  })
  .catch((error) => {
    /*There should be some logging service where we can pass this fetch failed error
    And some kind of notification component which can give some information to the user about failed
    scenario */
    console.log(error, 'fetch to properties api failed');
  }); 
}