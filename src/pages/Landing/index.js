import React, { Component } from 'react';
import Map from '../../components/Map';
import PropertyDetails from '../../components/PropertyDetails';
import FilterContainer from '../../components/FilterContainer';

export default class Home extends Component {
  render() {
    return(
      <div id='home'>
        <Map />
        <PropertyDetails />
        <FilterContainer title={"Council Filter"} filterField={"council"}/>
      </div>
    );
  }
};