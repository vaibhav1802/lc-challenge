import { EventEmitter } from 'fbemitter';

// Making sure that we have single instance of emitter object
export default (()=>{
  let emitter;
  return {
    getInstance() {
      if (!emitter) {
        emitter = new EventEmitter();
      }
      return emitter;
    }
  }
})();
