export function filterUniqueData (data, distinctProperty) {
  if (data.length) {
    return [...new Set(data.map((item) => item[distinctProperty]))];
  }
  return null;
}

export function filterOutData (data, appliedFilter) {
  let filteredData = [];
  if (appliedFilter.length) {
    data.forEach((item) => {
      appliedFilter.forEach((filter) => {
       if (item.council === filter) {
        filteredData.push(item);
       }
      });
    });
    return filteredData;
  }
  return data;
}