import {filterUniqueData, filterOutData} from './filter';

describe('Test unique set filter', () => {
  test('Check when data is empty', () => {
    let testData = [];
    let distinctProperty = 'council';
    expect(filterUniqueData(testData, distinctProperty)).toEqual(null);
  });

  test('Check when data is not empty', () => {
    let distinctProperty = 'council';
    let testData = [
      {
        "property_id": 1525674,
        "council": "CARDINIA",
        "council_property_number": "2188100300",
        "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
        "latitude": -37.9373447811622,
        "lga_code": 311,
        "longitude": 145.449895817713,
        "postcode": "3782"
      },
      {
        "property_id": 52858603,
        "council": "ALPINE",
        "council_property_number": "7567",
        "full_address": "BOGONG HIGH PLAINS ROAD BOGONG 3699",
        "latitude": -36.785034305,
        "lga_code": 300,
        "longitude": 147.19769273,
        "postcode": "3699"
      },
      {
        "property_id": 45453823,
        "council": "ALPINE",
        "council_property_number": null,
        "full_address": "UPPER HUMFFRAY ROAD SELWYN 3737",
        "latitude": -37.1029747328265,
        "lga_code": 300,
        "longitude": 146.941079580425,
        "postcode": "3737"
      }
    ];
    expect(filterUniqueData(testData, distinctProperty)).toEqual(["CARDINIA","ALPINE"]);
  });
});

describe('Test the filter out functionality', () => {
  test('Check when applied filter is empty', () => {
    let testData = [
      {
        "property_id": 1525674,
        "council": "CARDINIA",
        "council_property_number": "2188100300",
        "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
        "latitude": -37.9373447811622,
        "lga_code": 311,
        "longitude": 145.449895817713,
        "postcode": "3782"
      },
      {
        "property_id": 52858603,
        "council": "ALPINE",
        "council_property_number": "7567",
        "full_address": "BOGONG HIGH PLAINS ROAD BOGONG 3699",
        "latitude": -36.785034305,
        "lga_code": 300,
        "longitude": 147.19769273,
        "postcode": "3699"
      },
      {
        "property_id": 45453823,
        "council": "ALPINE",
        "council_property_number": null,
        "full_address": "UPPER HUMFFRAY ROAD SELWYN 3737",
        "latitude": -37.1029747328265,
        "lga_code": 300,
        "longitude": 146.941079580425,
        "postcode": "3737"
      }
    ];
    let appliedFilter = [];
    expect(filterOutData(testData, appliedFilter)).toEqual([
      {
        "property_id": 1525674,
        "council": "CARDINIA",
        "council_property_number": "2188100300",
        "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
        "latitude": -37.9373447811622,
        "lga_code": 311,
        "longitude": 145.449895817713,
        "postcode": "3782"
      },
      {
        "property_id": 52858603,
        "council": "ALPINE",
        "council_property_number": "7567",
        "full_address": "BOGONG HIGH PLAINS ROAD BOGONG 3699",
        "latitude": -36.785034305,
        "lga_code": 300,
        "longitude": 147.19769273,
        "postcode": "3699"
      },
      {
        "property_id": 45453823,
        "council": "ALPINE",
        "council_property_number": null,
        "full_address": "UPPER HUMFFRAY ROAD SELWYN 3737",
        "latitude": -37.1029747328265,
        "lga_code": 300,
        "longitude": 146.941079580425,
        "postcode": "3737"
      }
    ]);
  });

  test('Check when applied filter is not empty', () => {
    let appliedFilter = ["CARDINIA"];
    let testData = [
      {
        "property_id": 1525674,
        "council": "CARDINIA",
        "council_property_number": "2188100300",
        "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
        "latitude": -37.9373447811622,
        "lga_code": 311,
        "longitude": 145.449895817713,
        "postcode": "3782"
      },
      {
        "property_id": 52858603,
        "council": "ALPINE",
        "council_property_number": "7567",
        "full_address": "BOGONG HIGH PLAINS ROAD BOGONG 3699",
        "latitude": -36.785034305,
        "lga_code": 300,
        "longitude": 147.19769273,
        "postcode": "3699"
      },
      {
        "property_id": 45453823,
        "council": "ALPINE",
        "council_property_number": null,
        "full_address": "UPPER HUMFFRAY ROAD SELWYN 3737",
        "latitude": -37.1029747328265,
        "lga_code": 300,
        "longitude": 146.941079580425,
        "postcode": "3737"
      }
    ];
    expect(filterOutData(testData, appliedFilter)).toEqual(
    [{
    "property_id": 1525674,
    "council": "CARDINIA",
    "council_property_number": "2188100300",
    "full_address": "8 CLOVERLEIGH AVENUE EMERALD 3782",
    "latitude": -37.9373447811622,
    "lga_code": 311,
    "longitude": 145.449895817713,
    "postcode": "3782"
    }]);
  });
});