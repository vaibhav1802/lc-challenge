import React, { Component } from 'react';
import {HashRouter as Router} from 'react-router-dom';
import routes from '../routes';
import Header from '../components/Header';
import logo from '../assets/images/logo.svg';
import PropTypes from 'prop-types';

class App extends Component {
  render() {
    return (
      <div className="app-wrapper">
        <Header logo={logo} />
        <Router>
          {routes}
        </Router>
      </div>
    );
  }
}

App.propTypes = {
  logo: PropTypes.string
};

export default App;